tag := sutty/access_log

all: access_log

# Build only when source changes
access_log: src/access_log.cr
	crystal build --release $<
	strip --strip-all $@

alpine-build:
	docker build -t $(tag) .

cp:
	docker run -d $(tag) | while read container ; do \
		docker cp $$container:/root/access_log/access_log . ;\
		docker stop $$container ;\
		docker rm $$container ;\
	done

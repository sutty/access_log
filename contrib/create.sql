PRAGMA journal_mode=WAL;

CREATE TABLE IF NOT EXISTS "access_logs" (
 "id" NOT NULL PRIMARY KEY,
 "host" varchar DEFAULT NULL,
 "msec" float DEFAULT NULL,
 "server_protocol" varchar DEFAULT NULL,
 "request_method" varchar DEFAULT NULL,
 "request_completion" varchar DEFAULT NULL,
 "uri" varchar DEFAULT NULL,
 "query_string" varchar DEFAULT NULL,
 "status" integer DEFAULT NULL,
 "sent_http_content_type" varchar DEFAULT NULL,
 "sent_http_content_encoding" varchar DEFAULT NULL,
 "sent_http_etag" varchar DEFAULT NULL,
 "sent_http_last_modified" varchar DEFAULT NULL,
 "http_accept" varchar DEFAULT NULL,
 "http_accept_encoding" varchar DEFAULT NULL,
 "http_accept_language" varchar DEFAULT NULL,
 "http_pragma" varchar DEFAULT NULL,
 "http_cache_control" varchar DEFAULT NULL,
 "http_if_none_match" varchar DEFAULT NULL,
 "http_dnt" varchar DEFAULT NULL,
 "http_user_agent" varchar DEFAULT NULL,
 "http_origin" varchar DEFAULT NULL,
 "request_time" float DEFAULT NULL,
 "bytes_sent" integer DEFAULT NULL,
 "body_bytes_sent" integer DEFAULT NULL,
 "request_length" integer DEFAULT NULL,
 "http_connection" varchar DEFAULT NULL,
 "pipe" varchar DEFAULT NULL,
 "connection_requests" integer DEFAULT NULL,
 "geoip2_data_country_name" varchar DEFAULT NULL,
 "geoip2_data_city_name" varchar DEFAULT NULL,
 "ssl_server_name" varchar DEFAULT NULL,
 "ssl_protocol" varchar DEFAULT NULL,
 "ssl_early_data" varchar DEFAULT NULL,
 "ssl_session_reused" varchar DEFAULT NULL,
 "ssl_curves" varchar DEFAULT NULL,
 "ssl_ciphers" varchar DEFAULT NULL,
 "ssl_cipher" varchar DEFAULT NULL,
 "sent_http_x_xss_protection" varchar DEFAULT NULL,
 "sent_http_x_frame_options" varchar DEFAULT NULL,
 "sent_http_x_content_type_options" varchar DEFAULT NULL,
 "sent_http_strict_transport_security" varchar DEFAULT NULL,
 "nginx_version" varchar DEFAULT NULL,
 "pid" integer DEFAULT NULL,
 "remote_user" varchar DEFAULT NULL,
 "crawler" boolean DEFAULT 0,
 "http_referer" varchar DEFAULT NULL,
 "request_uri" varchar DEFAULT NULL,
 "datacenter_co2" float DEFAULT NULL,
 "network_co2" float DEFAULT NULL,
 "consumer_device_co2" float DEFAULT NULL,
 "production_co2" float DEFAULT NULL,
 "total_co2" float DEFAULT NULL
);

CREATE INDEX IF NOT EXISTS "index_access_logs_on_host" ON "access_logs" ("host");
CREATE INDEX IF NOT EXISTS "index_access_logs_on_uri" ON "access_logs" ("uri");
CREATE INDEX IF NOT EXISTS "index_access_logs_on_request_uri" ON "access_logs" ("request_uri");
CREATE INDEX IF NOT EXISTS "index_access_logs_on_status" ON "access_logs" ("status");
CREATE INDEX IF NOT EXISTS "index_access_logs_on_http_user_agent" ON "access_logs" ("http_user_agent");
CREATE INDEX IF NOT EXISTS "index_access_logs_on_geoip2_data_country_name" ON "access_logs" ("geoip2_data_country_name");
CREATE INDEX IF NOT EXISTS "index_access_logs_on_geoip2_data_city_name" ON "access_logs" ("geoip2_data_city_name");
CREATE INDEX IF NOT EXISTS "index_access_logs_on_http_origin" ON "access_logs" ("http_origin");

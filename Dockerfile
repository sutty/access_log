FROM sutty/sdk:latest

WORKDIR /root/access_log
RUN apk add --no-cache crystal
RUN apk add --no-cache sqlite-dev postgresql-dev zlib-dev
COPY . .
RUN crystal build --release --error-trace src/access_log.cr
RUN strip --strip-all access_log

require "spec"
require "../src/swd"

# XXX: Why was it a float number originally?
AVERAGE_WEBSITE_IN_BYTES = 2257715;

# XXX: The be_close values were taken from the co2.js tests
describe SWD do
  swd = SWD.new(AVERAGE_WEBSITE_IN_BYTES)

  describe "#new" do
    it "can be initialized" do
      swd.should(be_a(SWD))
    end
  end

  describe "#transfered_bytes_to_gb" do
    it "should convert bytes to gigabytes" do
      swd.transfered_bytes_to_gb.should(eq 0.002257715)
    end
  end

  describe "#energy_usage" do
    it "should convert gb to energy" do
      swd.energy_usage.should(eq 0.0027873024691358024)
    end
  end

  describe "#consumer_device_energy" do
    it "should extract the consumer device energy usage" do
      swd.consumer_device_energy.should(eq 0.0014493972839506174)
      swd.consumer_device_energy.should(be_close 0.00095095, 8)
    end
  end

  describe "#production_energy" do
    it "should extract the production energy usage" do
      swd.production_energy.should(eq 0.0005295874691358025)
      swd.production_energy.should(be_close 0.0003475, 7)
    end
  end

  describe "#network_energy" do
    it "should extract the network energy usage" do
      swd.network_energy.should(eq 0.0003902223456790124)
      swd.network_energy.should(be_close 0.00025602, 7)
    end
  end

  describe "#datacenter_energy" do
    it "should extract the datacenter energy usage" do
      swd.datacenter_energy.should(eq 0.00041809537037037033)
      swd.datacenter_energy.should(be_close 0.00027431, 8)
    end
  end

  describe "#total_energy" do
    it "should extract the total energy usage" do
      swd.total_energy.should(eq 0.0027873024691358024)
      swd.total_energy.should(be_close 0.00182874, 7)
    end
  end

  describe "#datacenter_carbon_intensity" do
    it "should be a global value by default" do
      swd.datacenter_carbon_intensity.should(eq SWD::GLOBAL_GRID_INTENSITY)
    end

    it "should be renewable when specified" do
      SWD.new(AVERAGE_WEBSITE_IN_BYTES, true).datacenter_carbon_intensity.should(eq SWD::RENEWABLES_GRID_INTENSITY)
    end

    it "can be initialized with a country iso code" do
      SWD.new(AVERAGE_WEBSITE_IN_BYTES, false, "AR").datacenter_carbon_intensity.should(eq SWD::IntensityData.by_iso("AR"))
    end
  end

  describe "#device_carbon_intensity" do
    it "should be a global value by default" do
      swd.device_carbon_intensity.should(eq SWD::GLOBAL_GRID_INTENSITY)
    end

    it "should be global even when datacenter uses renewable energy" do
      SWD.new(AVERAGE_WEBSITE_IN_BYTES, true).device_carbon_intensity.should(eq SWD::GLOBAL_GRID_INTENSITY)
    end

    it "can be initialized with a country iso code" do
      SWD.new(AVERAGE_WEBSITE_IN_BYTES, false, nil, "AR").device_carbon_intensity.should(eq SWD::IntensityData.by_iso("AR"))
    end
  end

  describe "#network_carbon_intensity" do
    it "should be a global value by default" do
      swd.network_carbon_intensity.should(eq SWD::GLOBAL_GRID_INTENSITY)
    end

    it "should be global even when datacenter uses renewable energy" do
      SWD.new(AVERAGE_WEBSITE_IN_BYTES, true).network_carbon_intensity.should(eq SWD::GLOBAL_GRID_INTENSITY)
    end
  end

  describe "#global_emissions" do
    it "should be a global value by default" do
      swd.global_emissions.should(eq SWD::GLOBAL_GRID_INTENSITY)
    end

    it "should be global even when datacenter uses renewable energy" do
      SWD.new(AVERAGE_WEBSITE_IN_BYTES, true).global_emissions.should(eq SWD::GLOBAL_GRID_INTENSITY)
    end
  end

  describe "#consumer_device_co2" do
    it "should extract the consumer device co2 emissions" do
      swd.consumer_device_co2.should(eq 0.6343432152938272)
    end
  end

  describe "#production_co2" do
    it "should extract the production co2 emissions" do
      swd.production_co2.should(eq 0.2317792517419753)
    end
  end

  describe "#network_co2" do
    it "should extract the network co2 emissions" do
      swd.network_co2.should(eq 0.1707847118098766)
    end
  end

  describe "#datacenter_co2" do
    it "should extract the datacenter co2 emissions" do
      swd.datacenter_co2.should(eq 0.1829836197962963)
    end
  end

  describe "#total_co2" do
    it "should extract the total co2 emissions" do
      swd.total_co2.should(eq 1.2198907986419754)
    end
  end
end

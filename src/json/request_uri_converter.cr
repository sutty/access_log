# Removes query string from request URI as we are already storing it in
# query_string.
module JSON::RequestUriConverter(Converter)
  def self.from_json(value : JSON::PullParser)
    value.read_string.split("?", 2).first
  end
end

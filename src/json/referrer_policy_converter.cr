require "../referrer_policy"

module String::Converter
  def self.from_json(value : JSON::PullParser)
    value.read_string
  end
end

# The referrer is sent as a Hash in the following format:
#
# { "http_referer": { "origin": "$http_origin", "referrer": "$http_referer", "policy": "same-origin" } }
module JSON::ReferrerPolicyConverter(Converter)
  def self.from_json(pull : JSON::PullParser)
    ReferrerPolicy.new(**self.options(pull)).referrer_from_policy
  end

  def self.to_json(value : String, builder : JSON::Builder)
  end

  # We only do this so we have double splats
  def self.options(pull)
    NamedTuple(policy: String, origin: String, referrer: String).from(Converter.from_json(pull).select(%w[policy origin referrer]))
  end
end

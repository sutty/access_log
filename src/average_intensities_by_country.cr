require "json"
require "csv"

csv = CSV.new(File.open("./countries_codes_and_coordinates.csv"), headers: true)
countries = Hash(String, String).new

while csv.next
  countries[csv["Alpha-3 code"]] = csv["Alpha-2 code"]
end

average_intensities = Hash(String, Float64).new

JSON.parse(ARGF.gets_to_end).as_h.each do |country, data|
  if data["country_code"].as_s.empty?
    STDERR.puts "not ok - Skipping #{country} because of missing ISO code"
    next
  end

  unless countries.has_key? data["country_code"].as_s
    STDERR.puts "not ok - Skipping #{country} because of missing ISO2 code"
    next
  end

  STDERR.puts "ok - #{country}"

  average_intensities[countries[data["country_code"].as_s]] =
    begin
      data["emissions_intensity_gco2_per_kwh"].as_f
    rescue TypeCastError
      data["emissions_intensity_gco2_per_kwh"].as_i.to_f
    end
end

puts average_intensities.to_json

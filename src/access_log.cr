require "log"
require "file_utils"
require "json"
require "socket"
require "sqlite3"
require "pg"
require "option_parser"
require "uuid"
require "system"
require "./models/access_log"
require "./models/crawler"
require "./swd"

VERSION = "0.5.8"

Log.setup_from_env

# Default socket location
socket = "/tmp/access_log.socket"
# The default database URI
database = "sqlite3://./development.sqlite3"
# Detect web crawlers
crawler = false
# Parse the crawlers repository
crawlers = [] of Crawler
# Fields in database
# TODO: Obtain them from AccessLog
fields = %w[id remote_user host msec server_protocol request_method request_completion uri request_uri query_string status sent_http_content_type sent_http_content_encoding sent_http_etag sent_http_last_modified http_accept http_accept_encoding http_accept_language http_pragma http_cache_control http_if_none_match http_dnt http_user_agent http_origin http_referer request_time bytes_sent body_bytes_sent request_length http_connection pipe connection_requests geoip2_data_country_name geoip2_data_city_name ssl_server_name ssl_protocol ssl_early_data ssl_session_reused ssl_curves ssl_ciphers ssl_cipher sent_http_x_xss_protection sent_http_x_frame_options sent_http_x_content_type_options sent_http_strict_transport_security nginx_version pid crawler datacenter_co2 network_co2 consumer_device_co2 production_co2 total_co2 node]
# Params for the query
params = [] of String
# SWD
swd = false
# Datacenter
datacenter : String? = nil
renewable = false
device_country = false
intensity = :marginal
node = false

# Parse CLI flags
OptionParser.parse do |p|
  p.banner = "Process JSON access logs"

  p.on "-v", "--version", "Show version" do
    puts VERSION
    exit
  end

  p.on "-h", "--help", "Show help" do
    puts p
    exit
  end

  # Change the database URI, we only support SQLite3 and PostgreSQL
  p.on "-d DATABASE", "--database=DATABASE", "Database URI" do |d|
    database = d
  end

  p.on "-c crawler-user-agents.json", "--crawlers crawler-user-agents.json", "Crawlers repository" do |c|
    crawler = true
    crawlers = Array(Crawler).from_json File.read(c)
  end

  p.on "-s /tmp/access_log.socket", "--socket /tmp/access_log.socket", "Listening socket" do |s|
    socket = s
  end

  p.on "-S", "--swd", "Enable Sustainable Web Design CO2 emission tracking" do
    swd = true
  end

  p.on "-D ISO2", "--datacenter ISO2", "Datacenter ISO 2-letter country code" do |d|
    datacenter = d
  end

  p.on "-R", "--renewable", "Datacenter uses renewable energy" do
    renewable = true
  end

  p.on "-C", "--device-country", "Take country of visit for SWD" do
    device_country = true
  end

  p.on "-A", "--average-intensity", "Use average intensity figures rather than marginal" do
    intensity = :average
  end

  p.on "-n", "--node", "Send node hostname" do
    node = true
  end
end

# Parameterize values according to database URI
if database.starts_with?("postgresql://") || database.starts_with?("postgres://")
  params = fields.map_with_index do |_, i|
    "$#{i + 1}"
  end
elsif database.starts_with? "sqlite3://"
  params = fields.map do |_|
    "?"
  end
end

# Generate the query
query = "insert into access_logs (#{fields.join(",")}) values (#{params.join(",")});"

# If we enabled crawler detection, concatenate all patterns as a single
# regex.
if crawler
  crawler_re = Regex.union(crawlers.map { |c| c.pattern })
end

server = Socket.unix(Socket::Type::DGRAM)
server.bind Socket::UNIXAddress.new(socket)

Signal::INT.trap do
  server.close
  FileUtils.rm(socket) if File.exists? socket
  exit
end

Signal::KILL.trap do
  server.close
  FileUtils.rm(socket) if File.exists? socket
  exit
end

# Open the database and wait for JSONL input.
db = DB.open database do |db|
  while true
    begin
      msg, _ = server.receive(1024 * 64) # 64K
      _, json = msg.split("{", 2)
      json = "{#{json}"

      # Parse input
      access_log = AccessLog.from_json(json || "{}")
      d = device_country ? access_log.geoip2_data_country_iso_code : nil
      s = SWD.new(access_log.bytes_sent, renewable, datacenter, d, intensity) if swd

      # Execute query, detect crawler if enabled.
      # TODO: Does Crystal support splats?  It does but we need to cast
      # AccessLog as a NamedTuple.
      db.exec query,
              UUID.random.to_s,
              access_log.remote_user,
              access_log.host,
              access_log.msec,
              access_log.server_protocol,
              access_log.request_method,
              access_log.request_completion,
              access_log.uri,
              access_log.request_uri,
              access_log.query_string,
              access_log.status,
              access_log.sent_http_content_type,
              access_log.sent_http_content_encoding,
              access_log.sent_http_etag,
              access_log.sent_http_last_modified,
              access_log.http_accept,
              access_log.http_accept_encoding,
              access_log.http_accept_language,
              access_log.http_pragma,
              access_log.http_cache_control,
              access_log.http_if_none_match,
              access_log.http_dnt,
              access_log.http_user_agent,
              access_log.http_origin,
              access_log.http_referer,
              access_log.request_time,
              access_log.bytes_sent,
              access_log.body_bytes_sent,
              access_log.request_length,
              access_log.http_connection,
              access_log.pipe,
              access_log.connection_requests,
              access_log.geoip2_data_country_name,
              access_log.geoip2_data_city_name,
              access_log.ssl_server_name,
              access_log.ssl_protocol,
              access_log.ssl_early_data,
              access_log.ssl_session_reused,
              access_log.ssl_curves,
              access_log.ssl_ciphers,
              access_log.ssl_cipher,
              access_log.sent_http_x_xss_protection,
              access_log.sent_http_x_frame_options,
              access_log.sent_http_x_content_type_options,
              access_log.sent_http_strict_transport_security,
              access_log.nginx_version,
              access_log.pid,
              (crawler ? !!(crawler_re =~ access_log.http_user_agent) : false),
              (swd ? s.try(&.datacenter_co2) : nil),
              (swd ? s.try(&.network_co2) : nil),
              (swd ? s.try(&.consumer_device_co2) : nil),
              (swd ? s.try(&.production_co2) : nil),
              (swd ? s.try(&.total_co2) : nil),
              (node ? System.hostname : nil)

    # Ignore parsing errors
    rescue e : JSON::ParseException
      Log.warn &.emit("Parse exception", error: e.message)
    rescue IO::Error
      server.close
      FileUtils.rm(socket) if File.exists? socket
      exit
    end
  end
end

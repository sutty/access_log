require "json"

class Crawler
  include JSON::Serializable

  property pattern : String
  property url : String?
  property instances : Array(String)
  property addition_date : String?
end

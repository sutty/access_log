require "json"
require "../json/referrer_policy_converter"
require "../json/request_uri_converter"

class AccessLog
  include JSON::Serializable

  property remote_user : String
  property host : String
  property msec : Float64
  property server_protocol : String
  property request_method : String
  property request_completion : String
  property uri : String

  @[JSON::Field(converter: JSON::RequestUriConverter(String::Converter))]
  property request_uri : String

  property query_string : String
  property status : Int32
  property sent_http_content_type : String
  property sent_http_content_encoding : String
  property sent_http_etag : String
  property sent_http_last_modified : String
  property http_accept : String
  property http_accept_encoding : String
  property http_accept_language : String
  property http_pragma : String
  property http_cache_control : String
  property http_if_none_match : String
  property http_dnt : String
  property http_user_agent : String
  property http_origin : String

  @[JSON::Field(converter: JSON::ReferrerPolicyConverter(JSON::HashValueConverter(String::Converter)))]
  property http_referer : String?

  property request_time : Float64
  property bytes_sent : Int64
  property body_bytes_sent : Int64
  property request_length : Int64
  property http_connection : String
  property pipe : String
  property connection_requests : Int32
  property geoip2_data_country_name : String
  property geoip2_data_country_iso_code : String?
  property geoip2_data_city_name : String
  property ssl_server_name : String
  property ssl_protocol : String
  property ssl_early_data : String
  property ssl_session_reused : String
  property ssl_curves : String
  property ssl_ciphers : String
  property ssl_cipher : String
  property sent_http_x_xss_protection : String
  property sent_http_x_frame_options : String
  property sent_http_x_content_type_options : String
  property sent_http_strict_transport_security : String
  property nginx_version : String
  property pid : String
end

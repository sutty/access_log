require "json"

marginal_intensities = Hash(String, Float32).new

JSON.parse(ARGF.gets_to_end).as_h.each do |country, data|
  if data["country_code_iso_2"].as_s.empty?
    STDERR.puts "not ok - Skipping #{country} because of missing ISO code"
    next
  end

  marginal_intensities[data["country_code_iso_2"].as_s] = data["Operating Margin Grid Emission"].as_s.to_f32
end

puts marginal_intensities.to_json

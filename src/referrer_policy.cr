# Processes different Referrer Policies according to 
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy
#
# We're assuming not every website and browser have privacy enhancing
# defaults, so the worst case is that the referrer field is always full,
# but some times it will be the origin or even empty.  We have to check
# if the referrer is empty so we don't get confused.
#
# The origin will be the site being visited.
class ReferrerPolicy
  getter policy : String
  getter origin : String
  getter referrer : String

  def initialize(policy : String = "strict-origin-when-cross-origin", origin : String = "", referrer : String = "")
    @policy = policy
    @origin = origin
    @referrer = referrer
  end

  # Applies a policy.
  #
  # TODO: Can we meta-program this?
  def referrer_from_policy
    case policy
    when "unsafe-url" then unsafe_url_policy
    when "no-referrer" then no_referrer_policy
    when "origin" then origin_policy
    when "origin-when-cross-origin" then origin_when_cross_origin_policy
    when "same-origin" then same_origin_policy
    when "strict-origin" then strict_origin_policy
    when "strict-origin-when-cross-origin" then strict_origin_when_cross_origin_policy
    else no_referrer_when_downgrade_policy
    end
  end

  # A referrer will be sent for same-site origins, but cross-origin
  # requests will send no referrer information.
  def same_origin_policy
    origin? ? referrer : ""
  end

  # Send the origin, path, and query string when performing a
  # same-origin request, but only send the origin of the document for
  # other cases.
  def origin_when_cross_origin_policy
    origin? ? referrer : origin_of_referrer
  end

  # Send the origin, path, and querystring when performing a
  # same-origin request, only send the origin when the protocol
  # security level stays the same while performing a cross-origin
  # request (HTTPS→HTTPS), and send no header to any less-secure
  # destinations (HTTPS→HTTP).
  def strict_origin_when_cross_origin_policy
    return "" if downgrade?

    origin? ? referrer : origin_of_referrer
  end

  # The Referer header will be omitted entirely. No referrer information
  # is sent along with requests.
  def no_referrer_policy
    ""
  end

  # This is the default behavior if no policy is specified, or if the
  # provided value is invalid. The origin, path, and querystring of the
  # URL are sent as a referrer when the protocol security level stays
  # the same (HTTP→HTTP, HTTPS→HTTPS) or improves (HTTP→HTTPS), but
  # isn't sent to less secure destinations (HTTPS→HTTP). 
  def no_referrer_when_downgrade_policy
    downgrade? ? "" : referrer
  end

  # Only send the origin of the document as the referrer.
  def origin_policy
    origin_of_referrer
  end

  # Only send the origin of the document as the referrer when the
  # protocol security level stays the same (HTTPS→HTTPS), but don't send
  # it to a less secure destination (HTTPS→HTTP).
  def strict_origin_policy
    downgrade? ? "" : origin_of_referrer
  end

  # Send the origin, path, and query string when performing any request,
  # regardless of security. 
  def unsafe_url_policy
    referrer
  end

  # Asks if referrer is from origin
  def origin?
    referrer.starts_with?(origin)
  end

  # Did the browser send the referer?
  def referrer?
    referrer != ""
  end

  # Is the destination insecure?  There's not much sense in asking this
  # question server-side.
  def downgrade?
    return false unless referrer?

    origin.starts_with?("https://") && referrer.starts_with?("http://")
  end

  # Extracts the referrer's origin
  def origin_of_referrer
    return "" unless referrer?

    referrer.split("/")[0..2].join("/")
  end
end
